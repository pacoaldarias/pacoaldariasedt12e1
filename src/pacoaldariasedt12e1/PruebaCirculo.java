/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacoaldariasedt12e1;

import java.text.DecimalFormat;
import javax.swing.JOptionPane;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class PruebaCirculo {

   public static void main(String[] args) {
      Circulo circulo = new Circulo(2.5, 37, 43);
      String salida = "La coordenada X es " + circulo.obtenerX() + "\nLa coordenada Y es " + circulo.obtenerY() + "\nEl radio es " + circulo.obtenerRadio();
      circulo.establecerX(35);
      circulo.establecerY(20);
      circulo.establecerRadio(4.25);
      salida += "\n\nLa nueva ubicacion y el radio del circulo son \n" + circulo.toString();
      DecimalFormat dosDigitos = new DecimalFormat("0.00");
      salida += "\nEl diametro es " + dosDigitos.format(circulo.obtenerDiametro());
      salida += "\nLa circunferencia es " + dosDigitos.format(circulo.obtenerCircunferencia());
      salida += "\nEl Area es " + dosDigitos.format(circulo.obtenerArea());
      JOptionPane.showMessageDialog(null, salida);
      System.exit(0);
   }
}
